# Development workflow
# - Executing nix-build builds the packages
# - Executing nix-shell returns a shell environment containing
#   1. Haskell packages needed
#   2. Hoogle files for packages
#   3. cabal and ghcid installed
{ pkgs ? import <nixpkgs> {} }:
pkgs.haskellPackages.developPackage {
  root = ./.;
  name = "ifupdown2";
  modifier = drv:
    # add buildtools such as cabal
    pkgs.haskell.lib.addBuildTools drv
      (with pkgs.haskellPackages; [
        cabal-install
        ghcid
        # install hoogle files for packages we use.
        (hoogleLocal {packages = drv.propagatedBuildInputs;})
      ]);
}
