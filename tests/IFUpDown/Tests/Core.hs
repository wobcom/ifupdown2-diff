{-# LANGUAGE OverloadedStrings #-}
module IFUpDown.Tests.Core (coreTests) where

import Data.Text (isInfixOf)
import Data.Text.Arbitrary
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.QuickCheck (testProperty)

import IFUpDown.Core (preprocess)

coreTests :: TestTree
coreTests = testGroup "Core"
  [ testProperty "preprocess removes escaped newlines" (not . isInfixOf "\\\n" . preprocess)
  ]
