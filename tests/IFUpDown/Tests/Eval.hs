module IFUpDown.Tests.Eval (evalTests) where

import Data.Text.Arbitrary
import Generic.Random
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.QuickCheck (testProperty, Arbitrary, arbitrary)

import IFUpDown.Eval (eval)
import IFUpDown.Types (Instruction, AutoIface, Interface, Range)

instance Arbitrary Range where
  arbitrary = genericArbitrary uniform

instance Arbitrary AutoIface where
  arbitrary = genericArbitrary uniform

instance Arbitrary Interface where
  arbitrary = genericArbitrary uniform

instance Arbitrary Instruction where
  arbitrary = genericArbitrary uniform

evalTests :: TestTree
evalTests = testGroup "Eval"
  [ testProperty "eval preserves the filename" (\s x -> s == fst (eval (s, x)))
  ]
