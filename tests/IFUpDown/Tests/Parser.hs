{-# LANGUAGE OverloadedStrings #-}
module IFUpDown.Tests.Parser (parserTests) where

import Data.Text (pack)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase, assertEqual)

import IFUpDown.Parser (parseInterfaces)

parserTests :: TestTree
parserTests = testGroup "Parser"
  [ testCase "empty file parses correctly" $
    assertEqual "empty interfaces file " (parseInterfaces ("", "")) (Right ("", []))
  ]
