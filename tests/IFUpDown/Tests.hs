module Main (main) where

import Test.Tasty (defaultMain, testGroup)

import IFUpDown.Tests.Core
import IFUpDown.Tests.Eval
import IFUpDown.Tests.Parser

main :: IO ()
main = defaultMain $ testGroup "Tests"
  [ coreTests
  , evalTests
  , parserTests
  ]
