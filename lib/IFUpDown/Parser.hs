{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE OverloadedStrings #-}
{-|
Module      : IFUpDown.Parser
Description : A Megaparsec parser for /etc/network/interfaces
Copyright   : (c) Wobcom GmbH, 2019-2020
License     : GPLv3
Maintainer  : veit@veitheller.de
Stability   : experimental
Portability : POSIX
-}
module IFUpDown.Parser (parseInterfaces) where

-- this parser is loosely based on ifupdown2 provided by Cumulus Networks:
-- https://github.com/CumulusNetworks/ifupdown2/blob/3b69ce6e62e801a1fb4f6ce4e5de9a1ead1350fe/ifupdown2/ifupdown/networkinterfaces.py

import Control.Monad (guard)
import Data.Data
import Data.Text (Text, isPrefixOf, length, unpack, pack)
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Data.Map as M
import qualified Data.Set as S
import qualified Text.Megaparsec.Char.Lexer as L

import IFUpDown.Types

newtype ParseErr = IfaceLengthError Text
  deriving (Eq, Data, Typeable, Ord, Read, Show)

instance ShowErrorComponent ParseErr where
  showErrorComponent (IfaceLengthError s) =
    "interface name too long: '" ++ unpack s ++ "', maximum is 16, got " ++ show (Data.Text.length s)

type Parser = Parsec ParseErr Text

keywords :: [Text]
keywords = ["vlan", "iface", "auto", "source"]

noKeyword :: Text -> Bool
noKeyword ident = not (elem ident keywords || isPrefixOf "allow-" ident)

identifier :: Parser Text
identifier = do
    parsed <- takeWhile1P (Just "identifier") notWhitespace
    guard (noKeyword parsed)
    return parsed
  where notWhitespace c = c /= ' ' && c/= '\n' && c/= '\t' && c /= '\r'

--sc :: Parser ()
--sc = L.space space1 (L.skipLineComment "#") neverP
--  where neverP = token (const Nothing) S.empty

sc :: Parser ()
sc = skipMany $ choice [space1, L.skipLineComment "#"]

source :: Parser Instruction
source = do
  _ <- string "source"
  _ <- takeWhile1P (Just "whitespace") (== ' ')
  filename <- identifier
  _ <- sc
  return (Source filename)

autoIface :: Parser AutoIface
autoIface = choice [allIface, regularIface, ifaceRange]
  where allIface = do
          _ <- string "all"
          return All
        -- we can either have a prefix range or a suffix range, not both
        regularIface = Regular <$> identifier
        ifaceRange = choice [prefixRange, suffixRange]
        range = do
          start <- some digitChar
          _ <- char '-'
          end <- some digitChar
          return (start, end)
        prefixRange = do
          ident <- identifier
          _ <- char '['
          (start, end) <- range
          _ <- string "]."
          suffix <- some digitChar

          return $ Ranged ident (Range (read start) (read end)) (Single (read suffix))
        suffixRange = do
          ident <- identifier
          prefix <- some digitChar
          _ <- string ".["
          (start, end) <- range
          _ <- char ']'

          return $ Ranged ident (Single (read prefix)) (Range (read start) (read end))

auto :: Parser Instruction
auto = do
  _ <- string "auto"
  _ <- some (char ' ')
  ifaces <- sepBy1 autoIface (some (char ' '))
  _ <- sc

  return (Auto (S.fromList ifaces))

allow :: Parser Instruction
allow = do
    _ <- string "allow-"
    allowClass <- identifier
    _ <- some (char ' ')
    ifaces <- sepBy1 identifier (some (char ' '))
    _ <- sc

    return (Allow allowClass (S.fromList ifaces))

ifaceInternal :: Text -> Parser Interface
ifaceInternal header = do
    _ <- string header
    _ <- some (char ' ')
    ident <- identifier
    _ <- many (char ' ')
    attributes <- sepBy identifier (some (char ' '))
    _ <- sc

    if Data.Text.length ident > 16
    then customFailure (IfaceLengthError ident)
    else do
      blines <- endBy ifaceBodyLine sc
      return Interface{ name=ident
                      , attrs=S.fromList attributes
                      , body=foldr insertLine M.empty blines
                      }
  where ifaceBodyLine = do
          key <- try identifier
          _ <- some (char ' ')
          vals <- endBy identifier (many (char ' '))
          return (key, S.fromList vals)
        insertLine (k, v) = M.insertWith S.union k v

iface :: Parser Instruction
iface = do
  ifc <- ifaceInternal "iface"

  return (Iface ifc)

vlan :: Parser Instruction
vlan = do
  ifc <- ifaceInternal "vlan"

  return (VLAN ifc)

instruction :: Parser Instruction
instruction = choice [ source
                     , auto
                     , allow
                     , iface
                     , vlan
                     ]

interfaces :: Parser [Instruction]
interfaces = do
  _ <- sc
  ifaces <- many instruction
  _ <- eof

  return ifaces

parseInterfaces :: (String, Text) -> Either String (Text, [Instruction])
parseInterfaces (file, contents) =
  case parse interfaces file contents of
    Left bundle -> Left (errorBundlePretty bundle)
    Right ifaces -> Right (pack file, ifaces)
