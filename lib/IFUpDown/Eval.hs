{-# LANGUAGE DeriveGeneric #-}
{-|
Module      : IFUpDown.Eval
Description : Evaluating the /etc/network/interfaces AST
Copyright   : (c) Wobcom GmbH, 2019-2020
License     : GPLv3
Maintainer  : veit@veitheller.de
Stability   : experimental
Portability : POSIX
-}
module IFUpDown.Eval (eval, IFDefinition(..)) where

import Data.List     (foldl')
import Data.Text     (Text)
import Data.TreeDiff (ToExpr)
import GHC.Generics  (Generic)
import qualified Data.Map.Strict as M
import qualified Data.Set as S

import IFUpDown.Types

data IFDefinition = IFDefinition {
  source :: S.Set Text,
  auto :: S.Set AutoIface,
  allow :: M.Map Text (S.Set Text),
  interfaces :: M.Map Text Interface,
  vlans :: S.Set Interface
}
  deriving (Show, Eq, Ord, Generic)

instance ToExpr IFDefinition

eval :: (Text, [Instruction]) -> (Text, IFDefinition)
eval (f, ins) = (f, foldl' eval' emptyDef ins)
  where emptyDef = IFDefinition{ allow=M.empty
                               , interfaces=M.empty
                               , vlans=S.empty
                               , auto=S.empty
                               , source=S.empty
                               }
        eval' d (Source s)  = d {source=S.insert s (source d)}
        eval' d (Auto a)    = d {auto=S.union (auto d) a}
        eval' d (Allow k v) = d {allow=M.insert k v (allow d)}
        eval' d (Iface i)   = d {interfaces=M.insert (name i) i (interfaces d)}
        eval' d (VLAN v)    = d {vlans=S.insert v (vlans d)}
