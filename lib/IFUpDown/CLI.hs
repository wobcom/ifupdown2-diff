{-# LANGUAGE ApplicativeDo   #-}
{-# LANGUAGE RecordWildCards #-}
{-|
Module      : IFUpDown.CLI
Description : Flag data types and parser
Copyright   : (c) Wobcom GmbH, 2019-2020
License     : GPLv3
Maintainer  : veit@veitheller.de
Stability   : experimental
Portability : POSIX

-}
module IFUpDown.CLI ( Flags(..)
                    , getFlags
                    )
where

import           Options.Applicative


data Flags = Flags {
  files :: [FilePath],
  highlight :: Bool
}

getFlags :: IO Flags
getFlags = execParser opts

opts :: ParserInfo Flags
opts = info (flags <**> helper)
    (fullDesc <> header "ifupdown2-parser — diffs /etc/network/interfaces files")

flags :: Parser Flags
flags = do
  highlight <- parseHighlight
  files <- parseFiles

  pure Flags {..}

parseFiles :: Parser [FilePath]
parseFiles = some (argument str (metavar "FILES..."))

parseHighlight :: Parser Bool
parseHighlight = switch ( long "highlight"
                       <> short 'l'
                       <> help "Whether to highlight the output")
