{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-|
Module      : IFUpDown.Core
Description : Utilities for running a diff of /etc/network/interfaces
Copyright   : (c) Wobcom GmbH, 2019-2020
License     : GPLv3
Maintainer  : veit@veitheller.de
Stability   : experimental
Portability : POSIX
-}
module IFUpDown.Core (run, preprocess) where

import Control.Monad (when)
import Data.Either
import Data.Text (Text, replace, intercalate, unpack)
import Data.Text.Prettyprint.Doc hiding (Pretty)
import Data.Text.Prettyprint.Doc.Render.Terminal
import Data.TreeDiff
import System.Exit
import System.IO
import qualified Data.Map.Strict as M
import qualified Data.Text.IO as TIO

import IFUpDown.CLI
import IFUpDown.Eval
import IFUpDown.Parser
import IFUpDown.Types (Interface(..))

-- | Our core function, should be called as CLI entrypoint
-- | It’s a little idiosyncratic, but basically it orchestrates the
-- | pipeline:
-- | CLI flags -> reading files -> preprocessing -> parsing -> diffing
-- |
-- | It will parse all the files, and if there were any errors parsing any of
-- | them, it will spit out those.
run :: IO ()
run = (readAll =<< getFlags) >>= parseAndDiff
  where readAll Flags{highlight, files} = do
            contents <- mapM readAndPair files
            return (highlight, contents)
        readAndPair f = do
          contents <- TIO.readFile f
          return (f, preprocess contents)
        parseAndDiff (hl, files) =
          let (errored, parsed) = partitionEithers (map parseInterfaces files)
          in if null errored
             then diff hl (map eval parsed)
             else do
               mapM_ (hPutStrLn stderr) errored
               exitFailure

p :: Pretty (Doc AnsiStyle)
p = Pretty
    { ppCon    = pretty
    , ppRec    = encloseSep lbrace rbrace comma
               . map (\(fn, d) -> pretty fn <+> fillSep [equals, d])
    , ppLst    = list
    , ppCpy    = id
    , ppIns    = \d -> annotate (color Green) (unAnnotate (pretty '+' <> d))
    , ppDel    = \d -> annotate (color Red) (unAnnotate (pretty '-' <> d))
    , ppSep    = sep
    , ppParens = parens
    , ppHang   = \d1 d2 -> indent 2 (fillSep [d1, d2])
    }


-- | Does intial preprocess on the interface file, as defined in
-- | https://github.com/CumulusNetworks/ifupdown2/blob/3b69ce6e62e801a1fb4f6ce4e5de9a1ead1350fe/ifupdown2/ifupdown/networkinterfaces.py#L414
preprocess :: Text -> Text
preprocess = replace "\\\n" " "

diff :: Bool -> [(Text, IFDefinition)] -> IO ()
diff _ [_] = hPutStrLn stderr "I was given only one file to diff?"
diff hl [(fa, a), (fb, b)] = diff'
  where render = if hl then renderHighlight else show . prettyEditExprCompact
        renderHighlight =
          unpack . renderStrict . layoutSmart defaultLayoutOptions . ppEditExprCompact p
        diff' = do
            diffProperty "VLANs" (vlans a) (vlans b)
            diffProperty "Sources" (source a) (source b)
            diffProperty "Auto" (auto a) (auto b)
            diffProperty "Allow" (allow a) (allow b)
            diffInterfaces
        diffProperty name ma mb =
            case ediff ma mb of
              Cpy (EditExp _) -> return ()
              edit            -> do putStrLn ("Diff in " ++ unpack name ++ ":")
                                    putStrLn (render edit)
        diffSets x y = (x M.\\ y, y M.\\ x, M.intersectionWith (,) x y)
        putIfEmpty m k s =
          when (M.empty /= m)
            (putStrLn
              ("Missing " ++ unpack k ++ " in " ++ unpack s ++ ": " ++ unpack (intercalate ", " (M.keys m))))
        conc = intercalate "."
        diffInterfaces =
          let (missA, missB, iBoth) = diffSets (interfaces a) (interfaces b)
          in do
            putIfEmpty missA "interfaces" fb
            putIfEmpty missB "interfaces" fa
            mapM_ diffInterface (M.toList iBoth)
        diffInterface (k, (ia, ib)) =
          do diffProperty (intercalate "" [k, ".attrs"]) (attrs ia) (attrs ib)
             diffBody k (body ia) (body ib)
        diffBody k ba bb =
          let (missA, missB, kBoth) = diffSets ba bb
          in do
            putIfEmpty missA "properties" (conc [fb, k])
            putIfEmpty missB "properties" (conc [fa, k])
            mapM_ (diffBodyElem k) (M.toList kBoth)
        diffBodyElem k (ik, (ba, bb)) = diffProperty (conc [k, ik]) ba bb
diff hl xs = mapM_ pairDiff pairs
  where pairDiff (x@(fx, _), y@(fy, _)) = do
          putStrLn ("Diffing " ++ unpack fx ++ " and " ++ unpack fy ++ ":")
          diff hl [x, y]
        pairs = [(x, y) | x <- xs, y <- xs, x /= y, x < y]
