{-# LANGUAGE DeriveGeneric #-}
{-|
Module      : IFUpDown.Types
Description : Types to represent /etc/network/interfaces data in Haskell
Copyright   : (c) Wobcom GmbH, 2019-2020
License     : GPLv3
Maintainer  : veit@veitheller.de
Stability   : experimental
Portability : POSIX
-}
module IFUpDown.Types ( Interface(..)
                      , Instruction(..)
                      , AutoIface(..)
                      , Range(..)
                      ) where

import Data.Map      (Map)
import Data.Set      (Set)
import Data.Text     (Text)
import Data.TreeDiff
import GHC.Generics  (Generic)

data Interface = Interface {
  name :: Text,
  attrs :: Set Text,
  body :: Map Text (Set Text)
}
  deriving (Show, Eq, Ord, Generic)

data Range = Single Int
           | Range Int Int
  deriving (Show, Eq, Ord, Generic)

data AutoIface = All
               | Ranged Text Range Range
               | Regular Text
  deriving (Show, Eq, Ord, Generic)

data Instruction = Source Text
                 | Auto (Set AutoIface)
                 | Allow Text (Set Text)
                 | Iface Interface
                 | VLAN Interface
  deriving (Show, Eq, Ord, Generic)

instance ToExpr Interface
instance ToExpr Range
instance ToExpr AutoIface
instance ToExpr Instruction
