# ifupdown2-diff

`ifupdown2-diff` is a program for diffing `/etc/network/interfaces` files. They
are parsed with Megaparsec and the diff is calculated and pretty-printed with
tree-diff.

The changes are diffed position-independently and grouping doesn’t matter. This
means that these two are equivalent:

```
allow-uplink swp1 swp2
allow-uplink swp3

# versus

allow-uplink swp1 swp3
allow-uplink swp2
```

## Build

The project assumes `cabal v2-*`, so it can be built using:

```
cabal v2-build
```

Running the script works as follows:

```
cabal v2-run exes
```

If your Cabal is new enough, `v2` is the default and `build`/`run` will be
enough.

## Run

```
ifupdown2-parser — diffs /etc/network/interfaces files

Usage: ifupdown2-diff [-l|--highlight] FILES...

Available options:
  -l,--highlight           Whether to highlight the output
  -h,--help                Show this help text
```

Basically you can diff two or more files using this tool. If you’re diffing
more than two, a pruned cartesian product (i.e. all the combinations of files
without duplicates) will be the basis of diffing. If you want to see the
changes highlighted in the terminal using ANSI escape sequences, use
`-l`/`--highlight`.

The output will be the difference in edit expressions. They should be fairly
readable.

# Development

As a default workflow you can use `cabal run`. It will recompile the project
and its dependencies on demand if necessary.

```
cabal run exes -- file1 file2
```

This is equivalent to building the program and then executing it the same
arguments like so:

```
ifupdown2-diff file1 file2
```

</hr>

Have fun!
